import java.util.Scanner;

public class GuessNumber {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String again;
        do{
        int answer = (int) (Math.random() * 100 + 1);
        int chances = 7;

        while (chances > 0) {
            System.out.println("请猜一个1-100之间的整数(含1和100)你还有"
                    + chances + "次机会,输入-1可结束本轮游戏");
            int guess;
            try{
                guess = scan.nextInt();
            }catch (Exception e){
                System.out.println("请输入合法数字");
                scan.next();
                continue;
            }
            if(guess==-1){
                break;
            }
            else if (guess > answer) {
                System.out.println("大了");
            } else if (guess < answer) {
                System.out.println("小了");
            } else {
                System.out.println("恭喜你！猜对啦！");
                break;
            }
            chances = chances - 1;
        }
        if (chances <= 0) {
            System.out.println("你没有机会啦！");
        }

        System.out.println("再玩一次吗？(y/n)");
         again = scan.next();
    }while(again.equalsIgnoreCase("y"));
    }
}
